import sys

import xbmcaddon
import xbmcplugin
from addon import Addon


def get_mac():
    def parse_mac(mac_addr):
        if mac_addr is None or mac_addr.strip() is '':
            return ''
        result = []
        ln = len(mac_addr)
        if ln == 12 or ln == 17:
            if ln == 12:
                st = 2
            else:
                st = 3
            for i in range(0, ln, st):
                result.append(mac_addr[i])
                result.append(mac_addr[i + 1])
                result.append(':')
        return ''.join(result)[:len(result) - 1]

    mac_orig = xbmcplugin.getSetting(handle, id='mac')
    mac = parse_mac(mac_orig)
    if mac == '':
        kodi_addon.openSettings()
    mac = parse_mac(xbmcplugin.getSetting(handle, id='mac'))
    if mac == '':
        sys.exit()
    elif mac_orig is not mac:
        xbmcplugin.setSetting(handle, id='mac', value=mac)
    return mac


url = sys.argv[0]
handle = int(sys.argv[1])
kodi_addon = xbmcaddon.Addon()
xbmcplugin.setContent(handle, 'tvshows')

addon = Addon(url, handle, get_mac(), kodi_addon)
addon.router(sys.argv[2])
